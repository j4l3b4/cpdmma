/**
 * The MusicBall class represents a bouncing ball that plays a sound upon a bounce.
 * The pitch of the sound is dependent on where (along the horizontal axis) the ball 
 * is released.
 */
class MusicBall
{
  float x, y;
  float radius;
  float colorRed, colorGreen, colorBlue, colorAlpha;
  float velocity, acceleration, friction;
  int   lifeSpan;
  
  // constructor
  MusicBall(float x, float y, int lifeSpan)
  {
    this.x = x;
    this.y = y;
    this.lifeSpan = lifeSpan;

    this.radius = 0.2 * (width / NUMBER_OF_NOTES);

    this.colorRed = random(127, 255);
    this.colorGreen = random(127, 255);
    this.colorBlue = random(127, 255);
    this.colorAlpha = 255;
    
    this.velocity = 4.0;
    this.acceleration = 0.4;
    this.friction = -0.8;
  }
  
  // return the note number of the ball (0-7)
  int getNote()
  {
    float noteWidth = width / NUMBER_OF_NOTES;
    int note = (int)(x / noteWidth);
    return note;
  }
  
  // call this method to display the ball
  void display()
  {
    stroke(0);
    fill(colorRed, colorGreen, colorBlue, colorAlpha);
    ellipse(x, y, 2 * radius, 2 * radius);
  }
  
  // call this method to update the state of the ball
  boolean update()
  {
    y += velocity;
    velocity = velocity + acceleration;
    return bounce();
  }
  
  /*
   * private methods
   */
  
  // determine if the ball has struct the botton of the display and should bounce
  boolean bounce()
  {
    boolean hasBounced = false;
 
    if (lifeSpan > 0) {
      if (y > height - radius) {
        y = height - radius;
        velocity = velocity * friction;
        
        colorAlpha *= 0.7;
        
        playSound();
        
        hasBounced = true;
        lifeSpan--;
      }
    }
    else {
      if (y > height + radius) {
        lifeSpan--;
      }
    }
    
    return hasBounced;
  }
  
  
  void playSound()
  {
    int note = getNote();
    audioPlayers[note].cue(0);
    audioPlayers[note].play();
  }
}
