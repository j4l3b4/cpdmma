/**
 * MusicalRain is a musical toy. The toy plays kalimba sounds and the mallets are
 * balls drawn by the user.
 *
 * Jim Budet
 * JimBudet@gmail.com
 */


Maxim maxim;

static final int NUMBER_OF_NOTES=8;
static final int MAXIMUM_NUMBER_OF_BALL_BOUNCES=8;
int distance = 0;
ArrayList<MusicBall> balls;
AudioPlayer[] audioPlayers = new AudioPlayer[8];

/**
 * The setup() function is called once when the program starts. 
 * It's used to define initial enviroment properties such as 
 * screen size and background color and to load media such as 
 * images and fonts as the program starts.
 */
void setup()
{
  // Define the dimension of the display window. Must be the first line of code inside setup(). 
  size(640, 640);
  
  // Set the color for the window background.
  background(0);
  
  maxim = new Maxim(this);
  
  balls = new ArrayList<MusicBall>();

  // load the sound files  
  for (int i=0; i<NUMBER_OF_NOTES; i++) {
    audioPlayers[i] = maxim.loadFile("kalimba-" + i + ".wav");
    audioPlayers[i].setLooping(false);
    audioPlayers[i].volume(0.5);
  }
}


/**
 * Called directly after setup(), the draw() function continuously 
 * executes the lines of code contained inside its block until the 
 * program is stopped or noLoop() is called.
 */
void draw()
{
  drawBackground();
  
  // display the balls
  for (MusicBall ball : balls) {
    ball.display();
  }

  // update the state of each ball and remote balls that are no longer playing
  ArrayList<MusicBall> expiredBalls = new ArrayList<MusicBall>();
  for (MusicBall ball : balls) {
    boolean hasBounced = ball.update();
    if (ball.lifeSpan < 0) {
      expiredBalls.add(ball);
    }
  }
  
  for (MusicBall ball : expiredBalls) {
    balls.remove(ball);
  }
}


/**
 * The mouseDragged() function is called once every time the mouse 
 * moves and a mouse button is pressed.
 */
void mouseDragged()
{  
  if (balls.isEmpty()) {
    MusicBall currentBall = new MusicBall(mouseX, mouseY, MAXIMUM_NUMBER_OF_BALL_BOUNCES);
    balls.add(currentBall);
  }
  else {
    MusicBall lastBall = balls.get(balls.size()-1);
    float distance = dist(lastBall.x, lastBall.y, mouseX, mouseY);
    if (distance > width/NUMBER_OF_NOTES) {
      MusicBall currentBall = new MusicBall(mouseX, mouseY, MAXIMUM_NUMBER_OF_BALL_BOUNCES);
      balls.add(currentBall);
    }
  }
}


/**
 * The mousePressed() function is called once after every time a 
 * mouse button is pressed. The mouseButton variable can be used 
 * to determine which button has been pressed.
 */
void mousePressed()
{
}


/**
 * The mouseClicked() function is called once after a mouse button 
 * has been pressed and then released.
 */
void mouseClicked()
{
  //println("mouseClicked");

  MusicBall currentBall = new MusicBall(mouseX, mouseY, MAXIMUM_NUMBER_OF_BALL_BOUNCES);
  balls.add(currentBall);
}


/**
 * The mouseReleased() function is called every time a mouse button 
 * is released.
 */
void mouseReleased()
{
}


/**
 * Helper method that draws the background for every frame. The background is divided
 * into eight vertical keys (0-7). Each key plays a different note. Each ball has
 * a color. The color of each key is the average of the ball colors for that key. If
 * there are no balls for a key, the key is drawn in black.
 *
 * The algorithm used to calculate each key color is horribly inefficient and should
 * be improved.
 */
void drawBackground()
{
  float rectWidth = width / NUMBER_OF_NOTES;
  float rectHeight = height;
  float rectX = 0;
  float rectY = 0;
  
  stroke(127, 127, 127);
  strokeWeight(3);

  for (int i=0; i<NUMBER_OF_NOTES; i++) {
    int n = 0;
    float red = 0, green = 0, blue = 0;
    for (MusicBall ball : balls) {
      int note = ball.getNote();
      if (note == i) {
        red += ball.colorRed * 0.5;
        green += ball.colorGreen * 0.5;
        blue += ball.colorBlue * 0.5;
        n++;
      }
    }

    if (n>0) 
      fill(red/n, green/n, blue/n, 64);
    else
      fill(255, 255, 255, 8);

    rect(rectX + rectWidth * i, rectY, rectWidth, rectHeight, 16);
  }


}
/**
 * The MusicBall class represents a bouncing ball that plays a sound upon a bounce.
 * The pitch of the sound is dependent on where (along the horizontal axis) the ball 
 * is released.
 */
class MusicBall
{
  float x, y;
  float radius;
  float colorRed, colorGreen, colorBlue, colorAlpha;
  float velocity, acceleration, friction;
  int   lifeSpan;
  
  // constructor
  MusicBall(float x, float y, int lifeSpan)
  {
    this.x = x;
    this.y = y;
    this.lifeSpan = lifeSpan;

    this.radius = 0.2 * (width / NUMBER_OF_NOTES);

    this.colorRed = random(127, 255);
    this.colorGreen = random(127, 255);
    this.colorBlue = random(127, 255);
    this.colorAlpha = 255;
    
    this.velocity = 4.0;
    this.acceleration = 0.4;
    this.friction = -0.8;
  }
  
  // return the note number of the ball (0-7)
  int getNote()
  {
    float noteWidth = width / NUMBER_OF_NOTES;
    int note = (int)(x / noteWidth);
    return note;
  }
  
  // call this method to display the ball
  void display()
  {
    stroke(255-colorRed, 225-colorGreen, 255-colorBlue);
    fill(colorRed, colorGreen, colorBlue, colorAlpha);
    ellipse(x, y, 2 * radius, 2 * radius);
  }
  
  // call this method to update the state of the ball
  boolean update()
  {
    y += velocity;
    velocity = velocity + acceleration;
    return bounce();
  }
  
  /*
   * private methods
   */
  
  // determine if the ball has struct the botton of the display and should bounce
  boolean bounce()
  {
    boolean hasBounced = false;
 
    if (lifeSpan > 0) {
      if (y > height - radius) {
        y = height - radius;
        velocity = velocity * friction;
        
        colorAlpha *= 0.7;
        
        playSound();
        
        hasBounced = true;
        lifeSpan--;
      }
    }
    else {
      if (y > height + radius) {
        lifeSpan--;
      }
    }
    
    return hasBounced;
  }
  
  
  void playSound()
  {
    int note = getNote();
    audioPlayers[note].stop();
    audioPlayers[note].cue(0);
    audioPlayers[note].play();
  }
}

