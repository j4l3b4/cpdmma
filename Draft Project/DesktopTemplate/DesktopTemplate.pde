//The MIT License (MIT) - See Licence.txt for details

//Copyright (c) 2013 Mick Grierson, Matthew Yee-King, Marco Gillies


Maxim maxim;

int distance = 0;
ArrayList points;

void setup()
{
  size(640, 960);
  
  background(0);


}


// code that happens every frame
void draw()
{
}


// code that happens when the mouse moves
// with the button down
void mouseDragged()
{
  int size = 30;
  ellipse(mouseX, mouseY, size, size);
}


// code that happens when the mouse button
// is pressed
void mousePressed()
{
  points = new ArrayList();
}


// code that happens when the mouse button
// is released
void mouseReleased()
{
}

